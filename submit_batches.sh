#!/bin/bash -l

for batch in $(ls -1 /home/mpib/pauley/CONOMY/batches/*.mat) ; do
name=LSS
echo '#!/bin/bash -l' > job.slurm
echo "#SBATCH --job-name $name" >> job.slurm
echo "#SBATCH --mem 8GB" >> job.slurm
echo "#SBATCH --output /home/mpib/pauley/logs/GLMs/slurm-%j.out" >> job.slurm
#echo "#SBATCH --mail-user pauley" >> job.slurm
#echo "#SBATCH --mail-type FAIL,END" >> job.slurm
echo "module load spm12" >> job.slurm
echo "run_spm12.sh /mcr batch $batch" >> job.slurm
sbatch job.slurm
done 
