library(lme4)
require(lmerTest)

# predict hit/miss from Age x Category Specificity interaction
model.h0 <- glmer(Memory~Age*CatSpec+(1|ID),
                  data = df, 
                  family = binomial,
                  control = glmerControl(optimizer = "bobyqa"),
                  nAGQ = 1)

# predict hit/miss from Age x Category Specificity interaction PLUS Age x Item Specificity
model.h1 <- glmer(Memory~Age*CatSpec+Age*ItemSpec+(1|ID),
                  data = df, 
                  family = binomial,
                  control = glmerControl(optimizer = "bobyqa"),
                  nAGQ = 1)

# compare model 1 to model 2
anova(model.h0,model.h1) 
