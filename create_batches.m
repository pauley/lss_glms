%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CREATE BATCHES FOR LSS GLMS
% 
% Script to speed up the process of creating Least Squares Separate GLMs
% for datasets with many trials. 
%
% Requires: SPM12 
% Inputs: 
%   1. list of subjects
%   2. files from create_regressors.m
% Output: 
%   1. One "batch file" per scanning run (per subject)
%
% Notes: This script was set up to analyze the CONOMY data (collected as
% part of Anna Karlsson's dissertation). CONOMY consisted of two sessions
% in the fMRI scanner ("pre" and "post"). Each session was divided into 3
% scanning runs in which objects were presented one-by-one separated by
% fixation crosses while participants completed a target-detection task.
%
% Author: Claire Pauley (assisted by Michael Krause)
% Max Planck Institute for Human Development
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% add spm12 to path
addpath('~/matlab/toolboxes/spm12/');

% initiate spm
spm('defaults','fmri');
spm_jobman('initcfg');

% set root path
root_server = '/home/beegfs/pauley/CONOMY/';

% path to folder containing output from create_regressors.m
spm_input = [root_server, 'spm_input/'];

% set (and create) output directory (for the final GLM output, not the
% batch files)
spm_output = [root_server, 'spm_output/'];
mkdir(spm_output)

% set path to nifti files of preprocessed fMRI data and brain masks
fmri_path = [root_server,'Data/'];

% load list of subjects
sub_list = dir([spm_input,'sub-*']);

% set (and create) output directory for the batch files
[~] = rmdir([root_server, 'batches'], 's');
mkdir([root_server, 'batches'])

for sub_id = 1:size(sub_list,1) % for each subject
    disp(['creating files for subject ', sub_list(sub_id).name])
    
    sub = sub_list(sub_id).name;
    
    for ses_id = 1:2 % for each scanning session
        
        if ses_id == 1
            ses = 'pre';
        elseif ses_id == 2
            ses = 'post';
        end
        
        for run = 1:3 % for each scanning run
            
            if run == 1
                num_obs = 106; % number of trials per run
                num_scans = 323; % number of volumes per run
            elseif run == 2 || run == 3
                num_obs = 107;
                num_scans = 327;
            end

            ob_count = 0; % trial counter
            for ob_id = 1:num_obs % for each trial
                
                % target trials were excluded, so this checks that the
                % current trial was not a target
                ob_exists = exist([spm_input,sub,'/ses-',ses,'/run_00',num2str(run),'_object_',num2str(ob_id),'_spm_conditions.mat']);
                
                if ob_exists ~= 0
                    % creates 3 new cells in the matlabbatch variable for
                    % each trial: one for model specification, one for model
                    % estimation, and one for contrast estimation
                    batch_offset = ob_count * 3;
                    ob_count = ob_count + 1;
                    
                    % creates output directory for each trial
                    % {root}/{subject}/{session}/{run}/{trial}
                    [~,~] = mkdir([spm_output,sub,'/ses-',ses,'/run-00',num2str(run),'/object-',num2str(ob_id)]);
                    
                    %========================================================================
                    % GENERAL SETUP
                    %========================================================================
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.dir = {[spm_output,sub,'/ses-',ses,'/run-00',num2str(run),'/object-',num2str(ob_id)]};
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.timing.units = 'secs';
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.timing.RT = 2.307;
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.timing.fmri_t = 96;
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.timing.fmri_t0 = 48;
                    
                    %========================================================================
                    % MODEL SPECIFICATION
                    %========================================================================
                    if run == 1
                        matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.sess(1).scans = cellstr(spm_select('ExtFPList', [fmri_path,sub,'/ses-',ses,'/fwhm_2'], 'task-object_run-001_space-T1w_desc-preproc_bold_.nii',1:num_scans));
                    elseif run == 2
                        matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.sess(1).scans = cellstr(spm_select('ExtFPList', [fmri_path,sub,'/ses-',ses,'/fwhm_2'], 'task-object_run-002_space-T1w_desc-preproc_bold_.nii',1:num_scans));
                    elseif run == 3
                        matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.sess(1).scans = cellstr(spm_select('ExtFPList', [fmri_path,sub,'/ses-',ses,'/fwhm_2'], 'task-object_run-003_space-T1w_desc-preproc_bold_.nii',1:num_scans));
                    end
                    
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.sess(1).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.sess(1).multi = {[spm_input,sub,'/ses-',ses,'/run_00',num2str(run),'_object_',num2str(ob_id),'_spm_conditions.mat']};
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.sess(1).multi_reg = {[spm_input,sub,'/ses-',ses,'/run_00',num2str(run),'_spm_regressors.mat']};
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.sess(1).hpf = 128;
                    
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.volt = 1;
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.global = 'None';
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.mthresh = 0.8;
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.mask = {[fmri_path,sub,'/ses-',ses,'/',sub,'_ses-',ses,'_task-object_run-00',num2str(run),'_space-T1w_desc-brain_mask.nii']};
                    matlabbatch{batch_offset + 1}.spm.stats.fmri_spec.cvi = 'AR(1)';
                    
                    
                    %========================================================================
                    % MODEL ESTIMATION
                    %========================================================================
                    
                    matlabbatch{batch_offset + 2}.spm.stats.fmri_est.spmmat = {[spm_output,sub,'/ses-',ses,'/run-00',num2str(run),'/object-',num2str(ob_id),'/SPM.mat']};
                    matlabbatch{batch_offset + 2}.spm.stats.fmri_est.write_residuals = 0;
                    matlabbatch{batch_offset + 2}.spm.stats.fmri_est.method.Classical = 1;
                    
                    
                    %========================================================================
                    % CONTRAST SETUP
                    %========================================================================
                    
                    matlabbatch{batch_offset + 3}.spm.stats.con.spmmat = {[spm_output,sub,'/ses-',ses,'/run-00',num2str(run),'/object-',num2str(ob_id),'/SPM.mat']};
                    matlabbatch{batch_offset + 3}.spm.stats.con.consess{1}.tcon.name = 'object-allobs';
                    matlabbatch{batch_offset + 3}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0 0];
                    matlabbatch{batch_offset + 3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
                    matlabbatch{batch_offset + 3}.spm.stats.con.delete = 0;
                    
                end
            end
        
        % saves batch file for each run    
        save([tempname([root_server, 'batches/']),'.mat'], 'matlabbatch');
 		clear matlabbatch ob_count batch_offset;
        end
    end
    
    clearvars -except root_server sub_id sub_list spm_input spm_output fmri_path
end
