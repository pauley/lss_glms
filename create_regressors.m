%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  CREATE REGRESSORS FOR LSS GLMS
% 
% Script to create condition and regressor files compatible with SPM12 in 
% order to run trial-wise Least Squares Separate GLMs. 
%
% Inputs: 
%   1. list of subjects
%   2. motion regressors - (in this case) tsv file from fmriprep output
%   3. information about trial onset times for each subject
% Output: 
%   1. folder titled 'spm_input' divided into subjects, sessions, and
    % runs
    % 2. each run folder contains an spm_conditions.mat file for every
    % object in the learning and baseline conditions and an
    % spm_regressors.mat file
    % 3. each spm_conditions.mat file contains the information to model
    % each trial individually using stick functions
    % 4. the spm_regressors.mat files contain information about the 6
    % motion regressors
    % (variables within spm_conditions.mat and spm_regressors.mat are
    % defined exactly as they are required to be compatible with SPM)
%
% Notes: This script was set up to analyze the CONOMY data (collected as
% part of Anna Karlsson's dissertation). CONOMY consisted of two sessions
% in the fMRI scanner ("pre" and "post"). Each session was divided into 3
% scanning runs in which objects were presented one-by-one separated by
% fixation crosses while participants completed a target-detection task.
%
% Author: Claire Pauley (assisted by Michael Krause)
% Max Planck Institute for Human Development
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
root = 'Z:/';

sub_list = dir([root,'CONOMY/subjects_to_add/sub-*']);

path_to_fmriprep = [root,'fmriprep_output/output/fmriprep/'];

spm_input = [root,'spm_input/'];

for sub_ind = 1:length(sub_list) % for each subject
    
    sub = sub_list(sub_ind).name;
    
    % loads file containing timing information for each trial
    outmat = load([root,'CONOMY/Output_table_format/',sub(8:15),'Output.mat']);
    
    for session = 1:2 % for each session
        if session == 1
            ses = 'ses-pre';
            
            mkdir([spm_input,sub,'/',ses]);
            
            % create spm_conditions.mat files for each trial which must contain 3 
            % variables:
            % names, onsets, and durations
            
            for ob = 1:size(outmat.Output_PreScan.preScan.OB.Results.dat,1)  % for each trial
                
                % target trials were excluded from analysis
                if outmat.Output_PreScan.preScan.OB.Results.dat.Target(ob) == 1
                    continue
                elseif outmat.Output_PreScan.preScan.OB.Results.dat.Target(ob) == 0
                    
                    if ob < 107 % indexes trials in run 1
                        rel_ob = ob;
                        run = 1;
                        % indexes onset times for all trials in the same
                        % run
                        onset_list = outmat.Output_PreScan.preScan.OB.time.dat(1:106,2) - outmat.Output_PreScan.RESU.run1.triggertime{1}(1);
                        % onset time of the current trial
                        ob_on = onset_list(rel_ob);
                        % onset times of all other trials in the same run
                        not_ob_on = onset_list;
                        not_ob_on(not_ob_on == ob_on | outmat.Output_PreScan.preScan.OB.Results.dat.Target(1:106) == 1) = [];
                    elseif ob > 106 && ob < 214 % indexes trials in run 2
                        rel_ob = ob - 106;
                        run = 2;
                        onset_list = outmat.Output_PreScan.preScan.OB.time.dat(107:213,2) - outmat.Output_PreScan.RESU.run2.triggertime{1}(1);
                        ob_on = onset_list(rel_ob);
                        not_ob_on = onset_list;
                        not_ob_on(not_ob_on == ob_on | outmat.Output_PreScan.preScan.OB.Results.dat.Target(107:213) == 1) = [];
                    elseif ob > 213 % run 3
                        rel_ob = ob - 213;
                        run = 3;
                        onset_list = outmat.Output_PreScan.preScan.OB.time.dat(214:end,2) - outmat.Output_PreScan.RESU.run3.triggertime{1}(1);
                        ob_on = onset_list(rel_ob);
                        not_ob_on = onset_list;
                        not_ob_on(not_ob_on == ob_on | outmat.Output_PreScan.preScan.OB.Results.dat.Target(214:end) == 1) = [];
                    end
                    
                    % durations of trials set to 0 for stick functions
                    ob_dur = 0;
                    not_ob_dur = zeros(1,length(not_ob_on));
                    
                    % variables formatted to spm
                    names = {'ob','not_ob'};
                    onsets = {ob_on,not_ob_on};
                    durations = {ob_dur,not_ob_dur};
                    
                    save([spm_input,sub,'/',ses,'/run_00',num2str(run),'_object_',num2str(rel_ob),'_spm_conditions.mat'],'names','onsets','durations');
                end
            end
            
        elseif session == 2
            ses = 'ses-post';
            
            mkdir([spm_input,sub,'/',ses]);
            
            
            for ob = 1:size(outmat.Output_PostScan.postScan.Results.dat,1)
                
                if outmat.Output_PostScan.postScan.Results.dat.Target(ob) == 1
                    continue
                elseif outmat.Output_PostScan.postScan.Results.dat.Target(ob) == 0
                    
                    if ob < 107
                        rel_ob = ob;
                        run = 1;
                        onset_list = outmat.Output_PostScan.postScan.time.dat(1:106,2) - outmat.Output_PostScan.RESU.run1.triggertime{1}(1);
                        ob_on = onset_list(rel_ob);
                        not_ob_on = onset_list;
                        not_ob_on(not_ob_on == ob_on | outmat.Output_PostScan.postScan.Results.dat.Target(1:106) == 1) = [];
                    elseif ob > 106 && ob < 214
                        rel_ob = ob - 106;
                        run = 2;
                        onset_list = outmat.Output_PostScan.postScan.time.dat(107:213,2) - outmat.Output_PostScan.RESU.run2.triggertime{1}(1);
                        ob_on = onset_list(rel_ob);
                        not_ob_on = onset_list;
                        not_ob_on(not_ob_on == ob_on | outmat.Output_PostScan.postScan.Results.dat.Target(107:213) == 1) = [];
                    elseif ob > 213
                        rel_ob = ob - 213;
                        run = 3;
                        onset_list = outmat.Output_PostScan.postScan.time.dat(214:end,2) - outmat.Output_PostScan.RESU.run3.triggertime{1}(1);
                        ob_on = onset_list(rel_ob);
                        not_ob_on = onset_list;
                        not_ob_on(not_ob_on == ob_on | outmat.Output_PostScan.postScan.Results.dat.Target(214:end) == 1) = [];
                    end
                    
                    ob_dur = 0;
                    not_ob_dur = zeros(1,length(not_ob_on));
                    
                    names = {'ob','not_ob'};
                    onsets = {ob_on,not_ob_on};
                    durations = {ob_dur,not_ob_dur};
                    
                    save([spm_input,sub,'/',ses,'/run_00',num2str(run),'_object_',num2str(rel_ob),'_spm_conditions.mat'],'names','onsets','durations');
                end
            end
            
        end
        
        % create regressors.mat files with the 6 motion regressors in 2
        % variables: R and names
        for run = 1:3

            % load file with motion regressors
            m = tdfread([path_to_fmriprep,sub,'/',ses,'/func/',sub,'_',ses,'_task-object_run-00',num2str(run),'_desc-confounds_regressors.tsv']);
            
            R = [];
            R(:,1) = m.rot_x;
            R(:,2) = m.rot_y;
            R(:,3) = m.rot_z;
            R(:,4) = m.trans_x;
            R(:,5) = m.trans_y;
            R(:,6) = m.trans_z;
            
            save([spm_input,sub,'/',ses,'/run_00',num2str(run),'_spm_regressors.mat'],'R','names');
        end
    end
end


